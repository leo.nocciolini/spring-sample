package br.com.leonocciolini.RestApi.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@NotEmpty(message = "Cannot be empty")
	@NotBlank(message = "Cannot be blank")
	@Size(min = 4, max = 255)
	@Column(name = "name")
	private String name;

	@Min(value = 0)
	@Max(value = 1000)
	@Column(name = "qtd")
	private Integer qtd;

	@Column(name = "created")
	private LocalDateTime created;

	public Product() {
		super();
	}

	public Product(String name, Integer qtd) {
		super();
		this.name = name;
		this.qtd = qtd;
	}

	@PrePersist
	public void onPrePersist() {
		if (created == null) {
			this.created = LocalDateTime.now();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQtd() {
		return qtd;
	}

	public void setQtd(Integer qtd) {
		this.qtd = qtd;
	}

	public LocalDateTime getCreate() {
		return created;
	}

	public void setCreate(LocalDateTime create) {
		this.created = create;
	}

	public String toString() {
		return "{name: " + name + ", qtd: " + qtd + ", created: "
				+ created.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:ii")) + "}";
	}

}
