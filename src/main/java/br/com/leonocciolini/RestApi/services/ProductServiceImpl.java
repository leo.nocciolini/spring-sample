package br.com.leonocciolini.RestApi.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.leonocciolini.RestApi.models.Product;
import br.com.leonocciolini.RestApi.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository repository;

	@Override
	public List<Product> findAll() {
		return this.repository.findAll();
	}

	@Override
	public Product findOne(Long id) {
		return this.repository.findById(id).get();
	}

	@Override
	public Product create(Product product) {
		repository.save(product);
		return product;

	}

	@Override
	public Product update(Product product, Long id) {
		if(repository.findById(id).isPresent()) {
			product.setId(id);
			repository.save(product);
			return product;
		}
		
		return null;
	}

	@Override
	public void destroy(Long id) {
		if (repository.findById(id).isPresent()) {
			repository.deleteById(id);
		}

	}

}
