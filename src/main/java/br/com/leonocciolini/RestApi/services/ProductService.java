package br.com.leonocciolini.RestApi.services;

import java.util.List;

import br.com.leonocciolini.RestApi.models.Product;

public interface ProductService {
	public List<Product> findAll();

	public Product findOne(Long id);

	public Product create(Product product);

	public Product update(Product product, Long id);

	public void destroy(Long id);

}
